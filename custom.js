var app = angular.module('app', ['ngRoute']);

app.config(function($routeProvider) {
	$routeProvider.when('/', {
		templateUrl: 'app.html',
		controller: 'ViewCtrl'
		,resolve : {
			loadData: viewCtrl.loadData
		}
	})
	.otherwise({
		redirectTo: '/'
	})
});

var appCtrl = app.controller('AppCtrl', function ($scope, $q, $route, $rootScope) {
	$rootScope.$on('$routeChangeError', function (event, current, previous, rejection) {
		console.log(event);
	})
});

var viewCtrl = app.controller('ViewCtrl', function ($scope, $q, $route) {
	console.log($route);
	$scope.model = {
		message: 'Home'
	};
});

viewCtrl.loadData = function ($q, $timeout) {
	var defer = $q.defer();

	$timeout(function () {
		defer.reject("Your network is down");
	}, 2000);
	return defer.promise;
}